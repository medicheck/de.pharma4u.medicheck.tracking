@servers(['jenkins' => '127.0.0.1'])

@story('deployintegration')
    prepare
    composerdev
    permissions
    phpunit
    laravel
    bundle
@endstory

@story('deploy')
    prepare
    composer
    assets
    permissions
    laravel
    bundle
@endstory

@story('test')
    composerdev
    permissions
    phpunit
@endstory

@task('prepare', ['on' => ['jenkins']])

    @if (file_exists('build.tar.gz'))
        rm build.tar.gz
    @endif

    @if (is_dir('build'))
        rm -r build
    @endif

    mkdir build
@endtask

@task('phpunit', ['on' => ['jenkins']])
    rm -r ./storage/phpunit/*
    ./vendor/bin/phpunit --testsuite "Unit" --coverage-html ./storage/phpunit
@endtask

@task('composerdev', ['on' => ['jenkins']])
    composer install
@endtask

@task('composer', ['on' => ['jenkins']])
    composer install --no-dev
@endtask

@task('assets', ['on' => ['jenkins']])
    npm install
    npm run prod
@endtask

@task('permissions', ['on' => ['jenkins']])
    chmod -R 0775 storage/
    chmod -R 0775 bootstrap/cache/
@endtask

@task('laravel', ['on' => ['jenkins']])
	php artisan view:clear
    php artisan config:clear
    php artisan route:clear
    php artisan key:generate
@endtask

@task('bundle', ['on' => ['jenkins']])
    tar pcfv build.tar app/ bootstrap/ config/ database/ public/ resources/ routes/ vendor/ .env artisan server.php composer.json composer.lock
    gzip build.tar
    mv build.tar.gz build/build.tar.gz
@endtask
